import { Component, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import {Ingredient} from "../../shared/ingredient.model";

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent {
  @ViewChild('nameInput') ingridientName: ElementRef;
  @ViewChild('amountInput') ingridientAmount: ElementRef;

  @Output() ingridientAddOut = new EventEmitter<Ingredient>();

  ingridientAdd() {
    const newIngredient = new Ingredient(this.ingridientName.nativeElement.value,this.ingridientAmount.nativeElement.value);
    this.ingridientAddOut.emit(newIngredient);
  }
}
