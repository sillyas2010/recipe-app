import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @Output() sectionChanged = new EventEmitter<string>();
  currentSection(section: string) {
    this.sectionChanged.emit(section);
  }
}
